<!DOCTYPE html>
<html lang="en">
<head>
    <title>Registration system</title>
    <!-- css -->
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <!-- <-- Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <!-- fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com"> 
    <link href="https://fonts.googleapis.com/css2?family=Noto+Sans+JP&family=Roboto:wght@300&display=swap" rel="stylesheet"> 
    <!-- Favicon link -->
    <link rel = "icon" href= "img/foto.png" type = "image/x-icon">
    <!-- Ajax -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/egg.js/1.0/egg.min.js"></script>
    </head>
<body>

<!-- heading block -->
<div class="container-fluid" id="headings">
    <h1><strong>User Registration System</strong></h1>
    <h3 style="color: #1e1f1f;">Sample Project 1</h3>
</div>

<!-- navigation bar -->
<!-- <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container-fluid">
        <button class="navbar-toggler" type="button" data-bs-toogle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
        </button>    
        <div class="collapse navbar-collapse" id="navbarSupportContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
               <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                  <li class="nav-item">
                  <a class="nav-link active" aria-current="page" href="index.php"><b>HOMEPAGE</b></a>
                  </li>

                  <li class="nav-item">
                  <a class="nav-link" href="display_user.php">Registered Users</a>
                  </li>
                 
                  <li class="nav-item">
                  <a class="nav-link" href="about.php">About</a>
                  </li>

                  
            </ul>
            <li class="d-flex">
               <a href="login.php"><Button class="btn btn-outline-success me-2 " type="submit">Login></button></a>
            </li>
            <li class="d-flex">
               <a href="signup.php"><button class="btn btn-outline-success me-2 " type="submit">Register</button></a>
            </li>
       </div>
    </div> 
</nav> -->


<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="container-fluid">
    
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
   
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
       
      <li class="nav-item">
                  <a class="nav-link active" aria-current="page" href="index.php"><b>HOMEPAGE</b></a>
                  </li>
      
                  <li class="nav-item">
                  <a class="nav-link" href="display_user.php">Registered Users</a>
                  </li>
        
                  <li class="nav-item">
                  <a class="nav-link" href="about.php">About</a>
                  </li>
      
    </ul>
      <form class="d-flex">
       
        <button class="btn btn-outline-success me-3 " type="submit" >Login</button>
       
      </form>

      <form class="d-flex">
       
       
       <button class="btn btn-outline-success" type="submit" >register</button>
     </form>
    </div>
  </div>
</nav>

<div id="main-para" style="padding-left: 7%;">
    <h2><b> Welcome!</b></h2>
    <br>
    <ul>
        <li>Sing up</li>
        <li>or register as new user.</li>
    </ul>
    <br>
    <!-- boxes -->
    <div class="row">
         <div class="col-sm-3">
            <div class="card"> 
                <div class="card-body">
                    <h5 class="card-title"><b>Users></b></h5>
                    <p class="card=text">View list of registered users</p>
                    <a href="display_user.php" class="btn btn-primary">Go</a>
                </div>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="card"> 
                <div class="card-body">
                    <h5 class="card-title"><b>Register</b></h5>
                    <p class="card=text">register as new users</p>
                    <a href="display_user.php" class="btn btn-primary">take me there</a>
                </div>
            </div>
        </div>

    </div> 
    <br><br><br><br><br>
</div>
<footer class="foot">
    <div><p style="margin: 0px; padding: 5px;">Thanks for visiting</p></div>
</footer> 
</body>
</html>